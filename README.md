# PREP
### Pull files from the repo

### Copy .env.example to .env (needs INSTALLED=false)

### Create `jeophpardy` DB on local
 
# Install
### Create Pyro 
```php artisan install --ready
composer update```
 (--r uses vars in .env)

### Create an addon (module):
```php artisan make:addon jeophpardy.module.jeophpardy```

### Create streams within the module:
```php artisan make:stream categories jeophpardy.module.jeophpardy

php artisan make:stream questions jeophpardy.module.jeophpardy```

### Install module and see it:
```php artisan addon:install jeophpardy.module.jeophpardy --seed```

# Generate the models in storage/app_ref/models 
```php artisan  streams:compile ```

# After this is complete, make sure INSTALLED=true in .env
# Cleanup
```php artisan assets:clear```

```php artisan cache:clear```

```php artisan view:clear```

```php artisan twig:clear```

#NEXT 
## As you build up the module, migrate the changes:
```php artisan addon:reinstall jeophpardy.module.jeophpardy --seed```