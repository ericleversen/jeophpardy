<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class JeophpardyModuleJeophpardyCreateCategoriesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'categories',
        'title_column' => 'name',
        'translatable' => true,
        'versionable' => false,
        'trashable' => true,
        'searchable' => false,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'translatable' => true,
            'required' => true,
        ],
        'source' => [
            'translatable' => false,
            'required' => true,
        ],
		'round'  => [
            'required' => true,
        ],
		'point_scale'  => [
            'required' => true,
        ],


    ];

}