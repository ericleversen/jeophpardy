<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class JeophpardyModuleJeophpardyCreateJeophpardyFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name' => 'anomaly.field_type.text',
        'source' => 'anomaly.field_type.text',
        'question_text' => [
	         "type"   => "anomaly.field_type.wysiwyg",
				"config" => [
	                "buttons"       => ["html", "format", "bold", "italic", "deleted", "lists", "link", 'underline'],
	                "plugins" => ["alignment",  "source" ],
	            ],
        ],
        'answer_text'    =>  'anomaly.field_type.text',
        'round' => 'anomaly.field_type.integer',
        'point_value' => 'anomaly.field_type.integer',
        'point_scale' => 'anomaly.field_type.integer',
        'name' =>  'anomaly.field_type.text',
		'category'	=> [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                "related" => 'Jeophpardy\JeophpardyModule\Category\CategoryModel',
            ]
        ],

    ];

}
