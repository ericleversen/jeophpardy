<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class JeophpardyModuleJeophpardyCreateQuestionsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'questions',
        'title_column' => 'question_text',
        'translatable' => true,
        'versionable' => false,
        'trashable' => true,
        'searchable' => false,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'question_text' => [
            'translatable' => true,
            'required' => true,
        ],
        'answer_text' => [
            'translatable' => true,
            'required' => true,
        ],
        'point_value'  => [
            'required' => true,
        ],
        'category'  => [
            'required' => true,
        ],

    ];
}
