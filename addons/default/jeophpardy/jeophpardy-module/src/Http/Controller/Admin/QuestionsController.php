<?php namespace Jeophpardy\JeophpardyModule\Http\Controller\Admin;

use Jeophpardy\JeophpardyModule\Question\Form\QuestionFormBuilder;
use Jeophpardy\JeophpardyModule\Question\Table\QuestionTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class QuestionsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param QuestionTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(QuestionTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param QuestionFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(QuestionFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param QuestionFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(QuestionFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
