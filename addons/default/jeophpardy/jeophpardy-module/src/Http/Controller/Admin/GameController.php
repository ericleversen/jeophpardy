<?php namespace Jeophpardy\JeophpardyModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Jeophpardy\JeophpardyModule\Question\QuestionModel;
use Jeophpardy\JeophpardyModule\Category\CategoryModel;

class GameController extends AdminController
{

    public function index(CategoryModel $category_model, $source=null, $round=null)
    {

	    	if (empty($source) ) {
			$categories = $category_model->where('source','longhorn2018')->where('round',1)->get();
		}
		else {

			$categories = $category_model->where('jeophpardy_categories.source',$source)->where('round',$round)->get();
		}
		$dailyDouble =  rand(2, 5) . rand(0, 5); // 'disabled';

		$game['header'] = $categories->pluck('name')->toArray();

		$row_count=0;
		foreach ($categories as $category) {
			$row = [];
			$col_count=0;
			foreach ($category->getQuestions()->sortBy('point_value') as $question) {
				$row['answer_text']=$question->answer_text;
				$row['question_text']=$question->question_text;
				$row['points']=$category->point_scale*$question->point_value;
				$row['class']= $dailyDouble === $row_count . $col_count ? 'tile dd' : 'tile';
				$game[$row['points']][]=$row;
				$col_count++;
			}
			$row_count++;
		}

		return view ('jeophpardy.module.jeophpardy::game',['game' => $game,]);
	}

}
