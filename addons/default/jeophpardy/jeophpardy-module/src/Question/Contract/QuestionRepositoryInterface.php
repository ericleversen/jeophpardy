<?php namespace Jeophpardy\JeophpardyModule\Question\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface QuestionRepositoryInterface extends EntryRepositoryInterface
{

}
