<?php namespace Jeophpardy\JeophpardyModule\Question;

use Jeophpardy\JeophpardyModule\Question\Contract\QuestionRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class QuestionRepository extends EntryRepository implements QuestionRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var QuestionModel
     */
    protected $model;

    /**
     * Create a new QuestionRepository instance.
     *
     * @param QuestionModel $model
     */
    public function __construct(QuestionModel $model)
    {
        $this->model = $model;
    }

      /**
     * Find an item by its ID
     *
     * @param $slug
     * @return mixed
     */
    public function findById($id)
    {
        return $this->model->where('jeophpardy_questions.id', $id)->first();
    }

}
