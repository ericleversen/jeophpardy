<?php namespace Jeophpardy\JeophpardyModule\Question;

use Jeophpardy\JeophpardyModule\Question\Contract\QuestionInterface;
use Anomaly\Streams\Platform\Model\Jeophpardy\JeophpardyQuestionsEntryModel;

class QuestionModel extends JeophpardyQuestionsEntryModel implements QuestionInterface
{


  	/**
     * @return \Anomaly\Streams\Platform\Entry\EntryPresenter|mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @param $category
     */
    public function source()
    {
		if ($this->getCategory()) {
	        return $this->getCategory()->source.' <br>Round '.$this->getCategory()->round;
	    }
	    else {
		    return 'n/a';
	    }
    }



}
