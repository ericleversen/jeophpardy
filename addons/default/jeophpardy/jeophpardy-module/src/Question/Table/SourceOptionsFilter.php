<?php namespace Jeophpardy\JeophpardyModule\Question\Table;

use Carbon\carbon;
use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Contract\SelectFilterInterface;
use Jeophpardy\JeophpardyModule\Category\CategoryModel;

/**
 * Class CoordinatorOrdersOrganizationFilterOptions
*/
class SourceOptionsFilter
{

    /**
     * Handle the options. Return the delivery dates for users'locaitons in past year
     *
     * @param FieldTypeCollection $fieldTypes
     */
    public function handle(CategoryModel $category_model, SelectFilterInterface $filter)
    {
		$categories=$category_model->all()->sortBy('source', SORT_NATURAL|SORT_FLAG_CASE);

        foreach($categories->unique('source') as $category) {
			$filtered[$category->source] = $category->source;
		}
        $filter->setOptions($filtered);

    }
}
