<?php namespace Jeophpardy\JeophpardyModule\Question\Table;

use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Contract\SelectFilterInterface;
use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Query\GenericFilterQuery;
use Illuminate\Database\Eloquent\Builder;


use Jeophpardy\JeophpardyModule\Category\CategoryModel;


/**
 * Class SearchIdFilter
 *
 * @link          http://pyrocms.com/
 * @author        EL
 */
class SourceQueryFilter
{

    /**
     * Handle the filter query.
     *
     * @param Builder         $query
     * @param FilterInterface $filter
     */
	public function handle(CategoryModel $category_model, Builder $query, SelectFilterInterface $filter)
    {
	    // get cat ids
	    $category_ids = $category_model->where('source', $filter->getValue())->get()->pluck('id');
		$query->whereIn('category_id', $category_ids);

    }
}
