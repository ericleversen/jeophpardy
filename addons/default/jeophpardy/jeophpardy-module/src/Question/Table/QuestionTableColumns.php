<?php namespace Jeophpardy\JeophpardyModule\Question\Table;


//use carbon\carbon;

/**
 * Class ActiveBasketTableColumns
 *
 */
class QuestionTableColumns
{

	protected $class;
    /**
     * Handle the form fields.
     *
     * @param ActiveBasketTableBuilder $builder
     */
    public function handle(QuestionTableBuilder $builder)
    {
        $fields = [
			[
                'heading' => 'Questions',
                'value'   => function ($entry) {
					return '<h4>'.$entry->answer_text.'</h4><br><b>'.$entry->question_text.'</b>';
                }
            ],

		    'category',
		    [
	            'heading' => 'Source',
				'value' => 'entry.source',
	        ],
	        'point_value',



        ];

        $builder->setColumns($fields);
    }
}
