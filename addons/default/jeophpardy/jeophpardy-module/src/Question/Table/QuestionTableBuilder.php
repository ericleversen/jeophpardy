<?php namespace Jeophpardy\JeophpardyModule\Question\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class QuestionTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
	    'question_text',
	    'answer_text',
		'category',
		'source' => [
			'filter'      => 'select',
            'query'       => SourceQueryFilter::class,
            'options'       => SourceOptionsFilter::class,
            'placeholder' => 'Source',

        ]
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
        protected $columns = QuestionTableColumns::class;


    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [
	     'order_by'   => [
            'point_value' => 'asc',
        ],
    ];

    /**
     * The table assets.
     *
     * @var array
     */
     protected $assets = [
	    'styles.css'  => [
            'jeophpardy.module.jeophpardy::css/small_images.css',
        ]
    ];

}
