<?php namespace Jeophpardy\JeophpardyModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Jeophpardy\JeophpardyModule\Question\Contract\QuestionRepositoryInterface;
use Jeophpardy\JeophpardyModule\Question\QuestionRepository;
use Anomaly\Streams\Platform\Model\Jeophpardy\JeophpardyQuestionsEntryModel;
use Jeophpardy\JeophpardyModule\Question\QuestionModel;
use Jeophpardy\JeophpardyModule\Category\Contract\CategoryRepositoryInterface;
use Jeophpardy\JeophpardyModule\Category\CategoryRepository;
use Anomaly\Streams\Platform\Model\Jeophpardy\JeophpardyCategoriesEntryModel;
use Jeophpardy\JeophpardyModule\Category\CategoryModel;
use Illuminate\Routing\Router;

class JeophpardyModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'game'           						=> 'Jeophpardy\JeophpardyModule\Http\Controller\Admin\GameController@index',
        'game/{id}/{round}'           			=> 'Jeophpardy\JeophpardyModule\Http\Controller\Admin\GameController@index',

        'admin/jeophpardy/questions'           	=> 'Jeophpardy\JeophpardyModule\Http\Controller\Admin\QuestionsController@index',
        'admin/jeophpardy/questions/create'    	=> 'Jeophpardy\JeophpardyModule\Http\Controller\Admin\QuestionsController@create',
        'admin/jeophpardy/questions/edit/{id}' 	=> 'Jeophpardy\JeophpardyModule\Http\Controller\Admin\QuestionsController@edit',
        'admin/jeophpardy'           			=> 'Jeophpardy\JeophpardyModule\Http\Controller\Admin\CategoriesController@index',
        'admin/jeophpardy/create'    			=> 'Jeophpardy\JeophpardyModule\Http\Controller\Admin\CategoriesController@create',
        'admin/jeophpardy/edit/{id}' 			=> 'Jeophpardy\JeophpardyModule\Http\Controller\Admin\CategoriesController@edit',
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Jeophpardy\JeophpardyModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Jeophpardy\JeophpardyModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Jeophpardy\JeophpardyModule\Event\ExampleEvent::class => [
        //    Jeophpardy\JeophpardyModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Jeophpardy\JeophpardyModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        JeophpardyQuestionsEntryModel::class => QuestionModel::class,
        JeophpardyCategoriesEntryModel::class => CategoryModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        QuestionRepositoryInterface::class => QuestionRepository::class,
        CategoryRepositoryInterface::class => CategoryRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
