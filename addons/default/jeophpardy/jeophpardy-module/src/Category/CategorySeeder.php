<?php namespace Jeophpardy\JeophpardyModule\Category;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

class CategorySeeder extends Seeder
{


    /**
     * Data to seed
     *
     * @var array
     */
    private $data = [

	];


    /**
     * @var CategoryRepository
     */
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        foreach ($this->data as $row) {
            if (!$this->repository->findById($row['id'])) {
                $this->repository->create($row);
            }
        }
    }
}
