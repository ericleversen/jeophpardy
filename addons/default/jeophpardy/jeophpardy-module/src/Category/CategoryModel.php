<?php namespace Jeophpardy\JeophpardyModule\Category;

use Jeophpardy\JeophpardyModule\Category\Contract\CategoryInterface;
use Anomaly\Streams\Platform\Model\Jeophpardy\JeophpardyCategoriesEntryModel;
use Jeophpardy\JeophpardyModule\Question\QuestionModel;


class CategoryModel extends JeophpardyCategoriesEntryModel implements CategoryInterface
{


    /**
     * Questions in each category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions(){
        return $this->hasMany(QuestionModel::class, 'category_id');
    }


    /**
     * Get the Location
     */
    public function getQuestions()
    {
        return $this->questions()->orderby('point_value')->get();
    }

}
