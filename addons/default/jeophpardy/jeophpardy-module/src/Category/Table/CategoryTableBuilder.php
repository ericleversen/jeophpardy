<?php namespace Jeophpardy\JeophpardyModule\Category\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class CategoryTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
	    'source',
	    'name',
	    'round',
	    'point_scale',
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = CategoryTableColumns::class;

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit',
		'play' => [
			'text' => 'Play',
			'icon' => 'fa fa-play',
            'type' => 'success',
            'href' => '/game/{entry.source}/{entry.round}'
        ],


    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
     protected $assets = [
	    'styles.css'  => [
            'jeophpardy.module.jeophpardy::css/small_images.css',
        ]
    ];

}
