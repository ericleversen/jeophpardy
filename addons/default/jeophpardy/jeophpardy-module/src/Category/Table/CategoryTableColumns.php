<?php namespace Jeophpardy\JeophpardyModule\Category\Table;


//use carbon\carbon;

/**
 * Class ActiveBasketTableColumns
 *
 */
class CategoryTableColumns
{

	protected $class;
    /**
     * Handle the form fields.
     *
     * @param ActiveBasketTableBuilder $builder
     */
    public function handle(CategoryTableBuilder $builder)
    {
        $fields = [
	        'name',
	        'source',
            [
                'heading' => 'Questions',
                'value'   => function ($entry) {

					$questions = $entry->getQuestions();
					$return="<ol>";
					foreach( $questions as $question) {

						$return.="<li>".$question->translate('en', true)->question_text."</li>";
						if (strlen($question->translate('en', true)->question_text) < 50) {
							$return.= "<br><br>";
						}

					}
                    return $return.'</ol><br>('.count($entry->getQuestions()).')';

                }
            ],
            [
                'heading' => 'Answers',
                'value'   => function ($entry) {

					$questions = $entry->getQuestions();
					$return="<ol>";
					foreach( $questions as $question) {
						$return.="<li>".$question->translate('en', true)->answer_text."</li>";
						if (strlen($question->translate('en', true)->answer_text) < 50) {
							$return.= "<br><br>";
						}
					}
                    return $return.'</ol><br>('.count($entry->getQuestions()).')';

                }
            ],
	        'round',
	        'point_scale',

        ];

        $builder->setColumns($fields);
    }
}
