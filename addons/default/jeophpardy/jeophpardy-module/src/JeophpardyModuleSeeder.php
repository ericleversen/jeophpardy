<?php namespace Jeophpardy\JeophpardyModule;


use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Jeophpardy\JeophpardyModule\Category\CategorySeeder;
use Jeophpardy\JeophpardyModule\Question\QuestionSeeder;

class JeophpardyModuleSeeder extends Seeder
{

    protected $seeders = [
        'Categories' => CategorySeeder::class,
		'Questions' => QuestionSeeder::class,
    ];

    /**
     * Seed the localization module.
     */
    public function run()
    {

        foreach ($this->seeders as $seeder) {
            $this->call($seeder);
        }
    }
}
