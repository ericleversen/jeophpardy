<?php

return [
    'categories' => [
        'read',
        'write',
        'delete',
    ],
    'questions' => [
        'read',
        'write',
        'delete',
    ],
];
