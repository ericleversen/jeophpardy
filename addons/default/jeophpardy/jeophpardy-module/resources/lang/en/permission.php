<?php

return [
    'categories' => [
        'name'   => 'Categories',
        'option' => [
            'read'   => 'Can read categories?',
            'write'  => 'Can create/edit categories?',
            'delete' => 'Can delete categories?',
        ],
    ],
    'questions' => [
        'name'   => 'Questions',
        'option' => [
            'read'   => 'Can read questions?',
            'write'  => 'Can create/edit questions?',
            'delete' => 'Can delete questions?',
        ],
    ],
];
