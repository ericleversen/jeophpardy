<?php

return [
	'name'          => [
        'name'         => 'Name',
        'label'        => 'Name',
    ],
	'source'          => [
        'name'         => 'Source',
        'label'        => 'Source',
    ],
    'category'          => [
        'name'         => 'Category',
        'label'        => 'Category',
    ],
	'slug'          => [
        'name'         => 'Slug',
        'label'        => 'Slug',
        'placeholder'  => '',
    ],

	'question_text'          => [
        'name'         => 'Question',
        'label'        => 'Question',
        'placeholder'  => '',
    ],

	'answer_text'          => [
        'name'         => 'Answer',
        'label'        => 'Answer',
        'placeholder'  => '',
    ],

	'point_value'          => [
        'name'         => 'Point Value',
        'label'        => 'Point Value',
        'placeholder'  => '',
        'instructions'		=> 'How many points is this question worth',
    ],

	'point_scale'          => [
        'name'         => 'Point Scale',
        'label'        => 'Point Scale',
        'placeholder'  => '',
        'instructions'		=> 'How many points is this round worth',
    ],


	'round'          => [
        'name'         => 'Round',
        'label'        => 'Round',
        'placeholder'  => '',
        'instructions'		=> 'Which Round does this category go with?',
    ],


];
