
# Create an addon (module type):
php artisan make:addon jeophpardy.module.jeophpardy

# Create a stream within your module:
php artisan make:stream categories jeophpardy.module.jeophpardy
php artisan make:stream questions jeophpardy.module.jeophpardy


# Install it using full addon notation (recommended):
php artisan addon:install jeophpardy.module.jeophpardy --seed

# Migrate changes as you build up the module:
php artisan addon:reinstall jeophpardy.module.jeophpardy --seed
